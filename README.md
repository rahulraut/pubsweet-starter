# PubSweet App

Welcome to the skeleton application!

## Quickstart

```bash
cd my-app-name
pubsweet setupdb (or npm run setupdb)
pubsweet start (or npm start)
```

## Customising the skeleton application

* Change `name` in `package.json` to your desired name
* Modify app config in the `/config` directory and webpack config in `/webpack`.
* Run `pubsweet add <components>` or `pubsweet remove <components>` to add or remove components. Mount these components on routes in `app/routes.jsx`
* Rewrite this README!
