import faker from 'faker'
import createDb from 'pubsweet-server/src/db'
import start from 'pubsweet/src/startup/start'
import { addUser, addCollection } from '@pubsweet/db-manager'

let server

export async function startServer() {
  if (!server) {
    server = await start()
  }
}

export async function setup(user, collection) {
  global.db = await createDb()

  const userData = user || {
    username: faker.internet.domainWord(),
    email: faker.internet.exampleEmail(),
    password: faker.internet.password(),
    admin: true,
  }

  const collectionData = collection || {
    title: faker.lorem.words(),
  }

  await addUser(userData)
  await addCollection(collectionData)

  return { userData, collectionData }
}

export async function teardown() {
  await global.db.destroy()
}
