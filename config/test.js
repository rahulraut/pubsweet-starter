const path = require('path')
const winston = require('winston')
const { deferConfig } = require('config/defer')

const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      level: process.env.DEBUG ? 'debug' : 'warn',
    }),
  ],
})

module.exports = {
  'pubsweet-server': {
    dbPath: path.join(__dirname, '..', 'api', 'db', 'test'),
    adapter: 'memory',
    logger,
    port: 4000,
    baseUrl: deferConfig(
      cfg => `http://localhost:${cfg['pubsweet-server'].port}`,
    ),
    secret: 'test',
  },
  'mail-transport': {
    port: 1025,
    auth: {
      user: 'user',
      pass: 'pass',
    },
  },
  'password-reset': {
    url: 'http://localhost:4000/password-reset',
  },
}
