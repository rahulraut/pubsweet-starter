module.exports = {
  'pubsweet-server': {
    dbPath: 'PUBSWEET_DB',
    sse: 'PUBSWEET_SSE',
    port: 'PORT',
  },
  watchTests: 'WATCH_TESTS',
}
